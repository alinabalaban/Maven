package com.company;

 class Cone {


     void getVolume(float r, float h) {
        final float Pi = 3.1415926535f;
        float Volume = Pi * (float) Math.pow(r, 2) * (h / 3);
        System.out.println("Cone Volume is " + Volume);
    }
}
